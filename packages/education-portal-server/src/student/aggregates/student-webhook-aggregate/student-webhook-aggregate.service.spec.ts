import { Test, TestingModule } from '@nestjs/testing';
import { StudentWebhookAggregateService } from './student-webhook-aggregate.service';
import { StudentService } from '../../../student/entity/student/student.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('StudentWebhookAggregateService', () => {
  let service: StudentWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StudentWebhookAggregateService,
        {
          provide: StudentService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StudentWebhookAggregateService>(
      StudentWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
