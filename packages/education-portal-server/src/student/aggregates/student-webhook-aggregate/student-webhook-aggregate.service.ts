import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import {
  StudentWebhookDto,
  GaurdiansWebhookDto,
} from '../../entity/student/student.webhook-dto';
import { StudentService } from '../../../student/entity/student/student.service';
import { Student } from '../../../student/entity/student/student.entity';
import { from, throwError } from 'rxjs';
import { switchMap, map, retry } from 'rxjs/operators';
import { FRAPPE_API_GET_STUDENT_ENDPOINT } from '../../../constants/routes';
import { STUDENT_ALREADY_EXISTS } from '../../../constants/messages';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class StudentWebhookAggregateService {
  constructor(
    private readonly studentService: StudentService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) {}

  studentCreated(studentPayload: StudentWebhookDto) {
    return from(
      this.studentService.findOne({
        name: studentPayload.name,
      }),
    ).pipe(
      switchMap(student => {
        if (student) {
          return throwError(new BadRequestException(STUDENT_ALREADY_EXISTS));
        }
        const provider = this.mapStudent(studentPayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.studentService
          .create(provider)
          .then(success => {})
          .catch(error => {});
        return this.syncStudentGuardian(provider);
      }),
    );
  }

  mapStudent(studentPayload: StudentWebhookDto) {
    const student = new Student();
    Object.assign(student, studentPayload);
    return student;
  }

  syncStudentGuardian(studentPayload: Student) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_STUDENT_ENDPOINT +
              studentPayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                map(res => res.data.data),
                switchMap(response => {
                  const guardian = this.mapGuradians(response.guardians);
                  return from(
                    this.studentService.updateOne(
                      { name: studentPayload.name },
                      {
                        $set: {
                          guardians: guardian,
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  mapGuradians(guardians: GaurdiansWebhookDto[]) {
    const sanitizedData = [];
    guardians.forEach(eachGuardian => {
      sanitizedData.push({
        guardian: eachGuardian.guardian,
        guardian_name: eachGuardian.guardian_name,
        relation: eachGuardian.relation,
      });
    });
    return sanitizedData;
  }

  studentDeleted(studentPayload: StudentWebhookDto) {
    this.studentService.deleteOne({ name: studentPayload.name });
  }

  studentUpdated(studentPayload: StudentWebhookDto) {
    return from(
      this.studentService.findOne({ name: studentPayload.name }),
    ).pipe(
      switchMap(student => {
        if (!student) {
          return this.studentCreated(studentPayload);
        }
        student.isSynced = true;
        this.studentService.updateOne(
          { name: student.name },
          { $set: studentPayload },
        );
        return this.syncStudentGuardian(student);
      }),
    );
  }
}
