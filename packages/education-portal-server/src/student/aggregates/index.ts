import { StudentAggregateService } from './student-aggregate/student-aggregate.service';
import { StudentWebhookAggregateService } from './student-webhook-aggregate/student-webhook-aggregate.service';

export const StudentAggregatesManager = [
  StudentAggregateService,
  StudentWebhookAggregateService,
];
