import { Test, TestingModule } from '@nestjs/testing';
import { ArticleWebhookAggregateService } from './article-webhook-aggregate.service';
import { ArticleService } from '../../../article/entity/article/article.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('ArticleWebhookAggregateService', () => {
  let service: ArticleWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleWebhookAggregateService,
        {
          provide: ArticleService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ArticleWebhookAggregateService>(
      ArticleWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
