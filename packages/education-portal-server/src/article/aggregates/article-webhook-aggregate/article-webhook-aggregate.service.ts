import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import { ArticleWebhookDto } from '../../entity/article/article-webhook-dto';
import { ArticleService } from '../../../article/entity/article/article.service';
import { Article } from '../../../article/entity/article/article.entity';
import { from, throwError } from 'rxjs';
import { switchMap, map, retry } from 'rxjs/operators';
import { FRAPPE_API_GET_ARTICLE_ENDPOINT } from '../../../constants/routes';
import { ARTICLE_ALREADY_EXISTS } from '../../../constants/messages';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class ArticleWebhookAggregateService {
  constructor(
    private readonly articleService: ArticleService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) {}

  articleCreated(articlePayload: ArticleWebhookDto) {
    return from(
      this.articleService.findOne({
        name: articlePayload.name,
      }),
    ).pipe(
      switchMap(article => {
        if (article) {
          return throwError(new BadRequestException(ARTICLE_ALREADY_EXISTS));
        }
        const provider = this.mapArticle(articlePayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.articleService
          .create(provider)
          .then(success => {})
          .catch(error => {});
        return this.syncGeneralChild(provider);
      }),
    );
  }

  mapArticle(articlePayload: ArticleWebhookDto) {
    const article = new Article();
    Object.assign(article, articlePayload);
    return article;
  }

  syncGeneralChild(articlePayload: Article) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_ARTICLE_ENDPOINT +
              articlePayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                map(res => res.data.data),
                switchMap(() => {
                  return from(
                    this.articleService.updateOne(
                      { name: articlePayload.name },
                      {
                        $set: {
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  articleDeleted(articlePayload: ArticleWebhookDto) {
    this.articleService.deleteOne({ name: articlePayload.name });
  }

  articleUpdated(articlePayload: ArticleWebhookDto) {
    return from(
      this.articleService.findOne({ name: articlePayload.name }),
    ).pipe(
      switchMap(article => {
        if (!article) {
          return this.articleCreated(articlePayload);
        }
        article.isSynced = true;
        this.articleService.updateOne(
          { name: article.name },
          { $set: articlePayload },
        );
        return this.syncGeneralChild(article);
      }),
    );
  }
}
