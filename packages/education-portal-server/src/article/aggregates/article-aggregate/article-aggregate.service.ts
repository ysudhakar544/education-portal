import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { ArticleDto } from '../../entity/article/article-dto';
import { Article } from '../../entity/article/article.entity';
import { ArticleAddedEvent } from '../../event/article-added/article-added.event';
import { ArticleService } from '../../entity/article/article.service';
import { ArticleRemovedEvent } from '../../event/article-removed/article-removed.event';
import { ArticleUpdatedEvent } from '../../event/article-updated/article-updated.event';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';

@Injectable()
export class ArticleAggregateService extends AggregateRoot {
  constructor(private readonly articleService: ArticleService) {
    super();
  }

  addArticle(articlePayload: ArticleDto, clientHttpRequest) {
    const article = new Article();
    Object.assign(article, articlePayload);
    article.uuid = uuidv4();
    this.apply(new ArticleAddedEvent(article, clientHttpRequest));
  }

  async retrieveArticle(uuid: string, req) {
    const provider = await this.articleService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getArticleList(offset, limit, sort, search, clientHttpRequest) {
    return await this.articleService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.articleService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new ArticleRemovedEvent(found));
  }

  async update(updatePayload: UpdateArticleDto) {
    const provider = await this.articleService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new ArticleUpdatedEvent(update));
  }
}
