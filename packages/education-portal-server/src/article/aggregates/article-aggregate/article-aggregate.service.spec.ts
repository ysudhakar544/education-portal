import { Test, TestingModule } from '@nestjs/testing';
import { ArticleAggregateService } from './article-aggregate.service';
import { ArticleService } from '../../entity/article/article.service';

describe('ArticleAggregateService', () => {
  let service: ArticleAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ArticleAggregateService,
        {
          provide: ArticleService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ArticleAggregateService>(ArticleAggregateService);
  });
  ArticleAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
