import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { ArticleWebhookDto } from '../../entity/article/article-webhook-dto';
import { ArticleWebhookAggregateService } from '../../aggregates/article-webhook-aggregate/article-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('article')
export class ArticleWebhookController {
  constructor(
    private readonly articleWebhookAggreagte: ArticleWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  articleCreated(@Body() articlePayload: ArticleWebhookDto) {
    return this.articleWebhookAggreagte.articleCreated(articlePayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  articleUpdated(@Body() articlePayload: ArticleWebhookDto) {
    return this.articleWebhookAggreagte.articleUpdated(articlePayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  articleDeleted(@Body() articlePayload: ArticleWebhookDto) {
    return this.articleWebhookAggreagte.articleDeleted(articlePayload);
  }
}
