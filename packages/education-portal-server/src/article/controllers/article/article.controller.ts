import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { ArticleDto } from '../../entity/article/article-dto';
import { AddArticleCommand } from '../../command/add-article/add-article.command';
import { RemoveArticleCommand } from '../../command/remove-article/remove-article.command';
import { UpdateArticleCommand } from '../../command/update-article/update-article.command';
import { RetrieveArticleQuery } from '../../query/get-article/retrieve-article.query';
import { RetrieveArticleListQuery } from '../../query/list-article/retrieve-article-list.query';
import { UpdateArticleDto } from '../../entity/article/update-article-dto';

@Controller('article')
export class ArticleController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() articlePayload: ArticleDto, @Req() req) {
    return this.commandBus.execute(new AddArticleCommand(articlePayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveArticleCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveArticleQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveArticleListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateArticleDto) {
    return this.commandBus.execute(new UpdateArticleCommand(updatePayload));
  }
}
