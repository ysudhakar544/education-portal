import { InjectRepository } from '@nestjs/typeorm';
import { Article } from './article.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article)
    private readonly articleRepository: MongoRepository<Article>,
  ) {}

  async find(query?) {
    return await this.articleRepository.find(query);
  }

  async create(article: Article) {
    const articleObject = new Article();
    Object.assign(articleObject, article);
    return await this.articleRepository.insertOne(articleObject);
  }

  async findOne(param, options?) {
    return await this.articleRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.articleRepository.manager.connection
      .getMetadata(Article)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.articleRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.articleRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.articleRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.articleRepository.updateOne(query, options);
  }
}
