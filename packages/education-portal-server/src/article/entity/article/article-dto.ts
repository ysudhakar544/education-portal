import { IsOptional, IsString, IsNumber } from 'class-validator';

export class ArticleDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  author: string;

  @IsOptional()
  @IsString()
  content: string;

  @IsOptional()
  @IsString()
  doctype: string;
}
