import { IsNotEmpty } from 'class-validator';
export class UpdateArticleDto {
  @IsNotEmpty()
  uuid: string;
}
