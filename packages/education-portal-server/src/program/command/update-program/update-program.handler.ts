import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateProgramCommand } from './update-program.command';
import { ProgramAggregateService } from '../../aggregates/program-aggregate/program-aggregate.service';

@CommandHandler(UpdateProgramCommand)
export class UpdateProgramCommandHandler
  implements ICommandHandler<UpdateProgramCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: ProgramAggregateService,
  ) {}

  async execute(command: UpdateProgramCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
