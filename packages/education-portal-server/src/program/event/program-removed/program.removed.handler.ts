import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { ProgramService } from '../../entity/program/program.service';
import { ProgramRemovedEvent } from './program-removed.event';

@EventsHandler(ProgramRemovedEvent)
export class ProgramRemovedCommandHandler
  implements IEventHandler<ProgramRemovedEvent> {
  constructor(private readonly programService: ProgramService) {}
  async handle(event: ProgramRemovedEvent) {
    const { program } = event;
    await this.programService.deleteOne({ uuid: program.uuid });
  }
}
