import { ProgramAddedCommandHandler } from './program-added/program-added.handler';
import { ProgramRemovedCommandHandler } from './program-removed/program.removed.handler';
import { ProgramUpdatedCommandHandler } from './program-updated/program-updated.handler';

export const ProgramEventManager = [
  ProgramAddedCommandHandler,
  ProgramRemovedCommandHandler,
  ProgramUpdatedCommandHandler,
];
