import { IsNotEmpty } from 'class-validator';
export class UpdateProgramDto {
  @IsNotEmpty()
  uuid: string;
}
