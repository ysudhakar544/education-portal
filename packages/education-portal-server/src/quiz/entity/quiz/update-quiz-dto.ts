import { IsNotEmpty } from 'class-validator';
export class UpdateQuizDto {
  @IsNotEmpty()
  uuid: string;
}
