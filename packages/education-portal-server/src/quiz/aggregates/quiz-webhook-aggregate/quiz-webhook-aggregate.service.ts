import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import {
  QuizWebhookDto,
  QuestionWebhookDto,
} from '../../entity/quiz/quiz-webhook-dto';
import { QuizService } from '../../../quiz/entity/quiz/quiz.service';
import { Quiz } from '../../../quiz/entity/quiz/quiz.entity';
import { from, throwError } from 'rxjs';
import { switchMap, map, retry } from 'rxjs/operators';
import { FRAPPE_API_GET_QUIZ_ENDPOINT } from '../../../constants/routes';
import { QUIZ_ALREADY_EXISTS } from '../../../constants/messages';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class QuizWebhookAggregateService {
  constructor(
    private readonly quizService: QuizService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) {}

  quizCreated(quizPayload: QuizWebhookDto) {
    return from(
      this.quizService.findOne({
        name: quizPayload.name,
      }),
    ).pipe(
      switchMap(quiz => {
        if (quiz) {
          return throwError(new BadRequestException(QUIZ_ALREADY_EXISTS));
        }
        const provider = this.mapQuiz(quizPayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.quizService
          .create(provider)
          .then(success => {})
          .catch(error => {});
        return this.syncGeneralChild(provider);
      }),
    );
  }

  mapQuiz(quizPayload: QuizWebhookDto) {
    const quiz = new Quiz();
    Object.assign(quiz, quizPayload);
    return quiz;
  }

  syncGeneralChild(quizPayload: Quiz) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_QUIZ_ENDPOINT +
              quizPayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                map(res => res.data.data),
                switchMap(response => {
                  const question = this.mapQuestion(response.question);
                  return from(
                    this.quizService.updateOne(
                      { name: quizPayload.name },
                      {
                        $set: {
                          question,
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  mapQuestion(question: QuestionWebhookDto[]) {
    const sanitizedData = [];
    question.forEach(eachQuestion => {
      sanitizedData.push({
        name: eachQuestion.name,
        docstatus: eachQuestion.docstatus,
        question_link: eachQuestion.question_link,
        question: eachQuestion.question,
        doctype: eachQuestion.doctype,
      });
    });
    return sanitizedData;
  }

  quizDeleted(quizPayload: QuizWebhookDto) {
    this.quizService.deleteOne({ name: quizPayload.name });
  }

  quizUpdated(quizPayload: QuizWebhookDto) {
    return from(this.quizService.findOne({ name: quizPayload.name })).pipe(
      switchMap(quiz => {
        if (!quiz) {
          return this.quizCreated(quizPayload);
        }
        quiz.isSynced = true;
        this.quizService.updateOne({ name: quiz.name }, { $set: quizPayload });
        return this.syncGeneralChild(quiz);
      }),
    );
  }
}
