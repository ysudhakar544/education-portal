import { Test, TestingModule } from '@nestjs/testing';
import { QuizWebhookAggregateService } from './quiz-webhook-aggregate.service';
import { QuizService } from '../../../quiz/entity/quiz/quiz.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('QuizWebhookAggregateService', () => {
  let service: QuizWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuizWebhookAggregateService,
        {
          provide: QuizService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuizWebhookAggregateService>(
      QuizWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
