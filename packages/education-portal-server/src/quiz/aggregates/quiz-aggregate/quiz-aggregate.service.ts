import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { QuizDto } from '../../entity/quiz/quiz-dto';
import { Quiz } from '../../entity/quiz/quiz.entity';
import { QuizAddedEvent } from '../../event/quiz-added/quiz-added.event';
import { QuizService } from '../../entity/quiz/quiz.service';
import { QuizRemovedEvent } from '../../event/quiz-removed/quiz-removed.event';
import { QuizUpdatedEvent } from '../../event/quiz-updated/quiz-updated.event';
import { UpdateQuizDto } from '../../entity/quiz/update-quiz-dto';

@Injectable()
export class QuizAggregateService extends AggregateRoot {
  constructor(private readonly quizService: QuizService) {
    super();
  }

  addQuiz(quizPayload: QuizDto, clientHttpRequest) {
    const quiz = new Quiz();
    Object.assign(quiz, quizPayload);
    quiz.uuid = uuidv4();
    this.apply(new QuizAddedEvent(quiz, clientHttpRequest));
  }

  async retrieveQuiz(uuid: string, req) {
    const provider = await this.quizService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getQuizList(offset, limit, sort, search, clientHttpRequest) {
    return await this.quizService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.quizService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new QuizRemovedEvent(found));
  }

  async update(updatePayload: UpdateQuizDto) {
    const provider = await this.quizService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new QuizUpdatedEvent(update));
  }
}
