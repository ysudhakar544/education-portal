import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { QuizDto } from '../../entity/quiz/quiz-dto';
import { AddQuizCommand } from '../../command/add-quiz/add-quiz.command';
import { RemoveQuizCommand } from '../../command/remove-quiz/remove-quiz.command';
import { UpdateQuizCommand } from '../../command/update-quiz/update-quiz.command';
import { RetrieveQuizQuery } from '../../query/get-quiz/retrieve-quiz.query';
import { RetrieveQuizListQuery } from '../../query/list-quiz/retrieve-quiz-list.query';
import { UpdateQuizDto } from '../../entity/quiz/update-quiz-dto';

@Controller('quiz')
export class QuizController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() quizPayload: QuizDto, @Req() req) {
    return this.commandBus.execute(new AddQuizCommand(quizPayload, req));
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveQuizCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveQuizQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveQuizListQuery(offset, limit, sort, search, clientHttpRequest),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateQuizDto) {
    return this.commandBus.execute(new UpdateQuizCommand(updatePayload));
  }
}
