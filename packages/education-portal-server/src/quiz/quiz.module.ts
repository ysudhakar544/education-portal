import { Module, HttpModule } from '@nestjs/common';
import { QuizAggregatesManager } from './aggregates';
import { QuizEntitiesModule } from './entity/entity.module';
import { QuizQueryManager } from './query';
import { CqrsModule } from '@nestjs/cqrs';
import { QuizCommandManager } from './command';
import { QuizEventManager } from './event';
import { QuizController } from './controllers/quiz/quiz.controller';
import { QuizPoliciesService } from './policies/quiz-policies/quiz-policies.service';
import { QuizWebhookController } from './controllers/quiz-webhook/quiz-webhook.controller';

@Module({
  imports: [QuizEntitiesModule, CqrsModule, HttpModule],
  controllers: [QuizController, QuizWebhookController],
  providers: [
    ...QuizAggregatesManager,
    ...QuizQueryManager,
    ...QuizEventManager,
    ...QuizCommandManager,
    QuizPoliciesService,
  ],
  exports: [QuizEntitiesModule],
})
export class QuizModule {}
