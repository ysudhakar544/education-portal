import { IQuery } from '@nestjs/cqrs';

export class RetrieveQuizQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
