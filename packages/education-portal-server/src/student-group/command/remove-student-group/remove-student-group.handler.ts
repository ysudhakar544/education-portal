import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { RemoveStudentGroupCommand } from './remove-student-group.command';
import { StudentGroupAggregateService } from '../../aggregates/student-group-aggregate/student-group-aggregate.service';

@CommandHandler(RemoveStudentGroupCommand)
export class RemoveStudentGroupCommandHandler
  implements ICommandHandler<RemoveStudentGroupCommand> {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: StudentGroupAggregateService,
  ) {}
  async execute(command: RemoveStudentGroupCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.remove(uuid);
    aggregate.commit();
  }
}
