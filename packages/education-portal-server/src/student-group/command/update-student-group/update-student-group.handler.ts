import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateStudentGroupCommand } from './update-student-group.command';
import { StudentGroupAggregateService } from '../../aggregates/student-group-aggregate/student-group-aggregate.service';

@CommandHandler(UpdateStudentGroupCommand)
export class UpdateStudentGroupCommandHandler
  implements ICommandHandler<UpdateStudentGroupCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: StudentGroupAggregateService,
  ) {}

  async execute(command: UpdateStudentGroupCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
