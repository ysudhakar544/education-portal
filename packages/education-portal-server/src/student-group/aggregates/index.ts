import { StudentGroupAggregateService } from './student-group-aggregate/student-group-aggregate.service';
import { StudentGroupWebhookAggregateService } from './student-group-webhook-aggregate/student-group-webhook-aggregate.service';

export const StudentGroupAggregatesManager = [
  StudentGroupAggregateService,
  StudentGroupWebhookAggregateService,
];
