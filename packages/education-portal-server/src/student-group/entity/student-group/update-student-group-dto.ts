import { IsNotEmpty } from 'class-validator';
export class UpdateStudentGroupDto {
  @IsNotEmpty()
  uuid: string;
}
