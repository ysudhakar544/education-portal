import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class StudentGroup extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  academic_year: string;

  @Column()
  group_based_on: string;

  @Column()
  student_group_name: string;

  @Column()
  max_strength: number;

  @Column()
  academic_term: string;

  @Column()
  program: string;

  @Column()
  course: string;

  @Column()
  disabled: number;

  @Column()
  doctype: string;

  @Column()
  students: GroupStudents[];

  @Column()
  instructors: Insructors[];

  @Column()
  isSynced: boolean;
}
export class GroupStudents {
  name: string;
  docstatus: number;
  student: string;
  student_name: string;
  group_roll_number: number;
  active: number;
  doctype: string;
}
export class Insructors {
  name: string;
  docstatus: number;
  instructor: string;
  instructor_name: string;
  doctype: string;
}
