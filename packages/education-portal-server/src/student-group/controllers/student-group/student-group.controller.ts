import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { StudentGroupDto } from '../../entity/student-group/student-group-dto';
import { AddStudentGroupCommand } from '../../command/add-student-group/add-student-group.command';
import { RemoveStudentGroupCommand } from '../../command/remove-student-group/remove-student-group.command';
import { UpdateStudentGroupCommand } from '../../command/update-student-group/update-student-group.command';
import { RetrieveStudentGroupQuery } from '../../query/get-student-group/retrieve-student-group.query';
import { RetrieveStudentGroupListQuery } from '../../query/list-student-group/retrieve-student-group-list.query';
import { UpdateStudentGroupDto } from '../../entity/student-group/update-student-group-dto';

@Controller('student_group')
export class StudentGroupController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() studentGroupPayload: StudentGroupDto, @Req() req) {
    return this.commandBus.execute(
      new AddStudentGroupCommand(studentGroupPayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  // @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveStudentGroupCommand(uuid));
  }

  @Get('v1/get/:uuid')
  // @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(
      new RetrieveStudentGroupQuery(uuid, req),
    );
  }

  @Get('v1/list')
  // @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveStudentGroupListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  // @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateStudentGroupDto) {
    return this.commandBus.execute(
      new UpdateStudentGroupCommand(updatePayload),
    );
  }
}
