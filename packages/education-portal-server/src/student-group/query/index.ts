import { RetrieveStudentGroupQueryHandler } from './get-student-group/retrieve-student-group.handler';
import { RetrieveStudentGroupListQueryHandler } from './list-student-group/retrieve-student-group-list.handler';

export const StudentGroupQueryManager = [
  RetrieveStudentGroupQueryHandler,
  RetrieveStudentGroupListQueryHandler,
];
