import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CourseUpdatedEvent } from './course-updated.event';
import { CourseService } from '../../entity/course/course.service';

@EventsHandler(CourseUpdatedEvent)
export class CourseUpdatedCommandHandler
  implements IEventHandler<CourseUpdatedEvent> {
  constructor(private readonly object: CourseService) {}

  async handle(event: CourseUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
