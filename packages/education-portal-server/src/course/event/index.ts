import { CourseAddedCommandHandler } from './course-added/course-added.handler';
import { CourseRemovedCommandHandler } from './course-removed/course.removed.handler';
import { CourseUpdatedCommandHandler } from './course-updated/course-updated.handler';

export const CourseEventManager = [
  CourseAddedCommandHandler,
  CourseRemovedCommandHandler,
  CourseUpdatedCommandHandler,
];
