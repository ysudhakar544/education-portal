import {
  Controller,
  Post,
  Body,
  UsePipes,
  UseGuards,
  ValidationPipe,
} from '@nestjs/common';
import { CourseWebhookDto } from '../../entity/course/course-webhook-dto';
import { CourseWebhookAggregateService } from '../../aggregates/course-webhook-aggregate/course-webhook-aggregate.service';
import { FrappeWebhookGuard } from '../../../auth/guards/frappe-webhook.guard';

@Controller('course')
export class CourseWebhookController {
  constructor(
    private readonly courseWebhookAggreagte: CourseWebhookAggregateService,
  ) {}
  @Post('webhook/v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  courseCreated(@Body() coursePayload: CourseWebhookDto) {
    return this.courseWebhookAggreagte.courseCreated(coursePayload);
  }

  @Post('webhook/v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  courseUpdated(@Body() coursePayload: CourseWebhookDto) {
    return this.courseWebhookAggreagte.courseUpdated(coursePayload);
  }

  @Post('webhook/v1/delete')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  @UseGuards(FrappeWebhookGuard)
  courseDeleted(@Body() coursePayload: CourseWebhookDto) {
    return this.courseWebhookAggreagte.courseDeleted(coursePayload);
  }
}
