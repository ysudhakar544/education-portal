import { Test, TestingModule } from '@nestjs/testing';
import { CourseAggregateService } from './course-aggregate.service';
import { CourseService } from '../../entity/course/course.service';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';

describe('CourseAggregateService', () => {
  let service: CourseAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CourseAggregateService,
        {
          provide: CourseService,
          useValue: {},
        },
        {
          provide: TeacherService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<CourseAggregateService>(CourseAggregateService);
  });
  CourseAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
