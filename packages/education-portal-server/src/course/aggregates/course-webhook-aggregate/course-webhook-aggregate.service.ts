import { Injectable, BadRequestException } from '@nestjs/common';
import { CourseWebhookDto } from '../../entity/course/course-webhook-dto';
import { CourseService } from '../../entity/course/course.service';
import { Course } from '../../entity/course/course.entity';
import { from, throwError, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { COURSE_ALREADY_EXISTS } from '../../../constants/messages';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class CourseWebhookAggregateService {
  constructor(private readonly courseService: CourseService) {}

  courseCreated(coursePayload: CourseWebhookDto) {
    return from(
      this.courseService.findOne({
        name: coursePayload.name,
      }),
    ).pipe(
      switchMap(course => {
        if (course) {
          return throwError(new BadRequestException(COURSE_ALREADY_EXISTS));
        }
        const provider = this.mapCourse(coursePayload);
        this.courseService
          .create(provider)
          .then(success => {})
          .catch(error => {});
        return of({});
      }),
    );
  }

  mapCourse(coursePayload: CourseWebhookDto) {
    const course = new Course();
    Object.assign(course, coursePayload);
    course.uuid = uuidv4();
    course.isSynced = true;
    return course;
  }

  courseDeleted(coursePayload: CourseWebhookDto) {
    this.courseService.deleteOne({ name: coursePayload.name });
  }

  courseUpdated(coursePayload: CourseWebhookDto) {
    return from(this.courseService.findOne({ name: coursePayload.name })).pipe(
      switchMap(course => {
        if (!course) {
          return this.courseCreated(coursePayload);
        }
        course.isSynced = true;
        this.courseService.updateOne(
          { name: course.name },
          { $set: coursePayload },
        );
        return of({});
      }),
    );
  }
}
