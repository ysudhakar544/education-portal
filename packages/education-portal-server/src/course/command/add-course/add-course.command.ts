import { ICommand } from '@nestjs/cqrs';
import { CourseDto } from '../../entity/course/course-dto';

export class AddCourseCommand implements ICommand {
  constructor(
    public coursePayload: CourseDto,
    public readonly clientHttpRequest: any,
  ) {}
}
