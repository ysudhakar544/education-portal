import {
  IsString,
  IsOptional,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CourseDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsString()
  @IsOptional()
  course_name: string;

  @IsString()
  @IsOptional()
  department: string;

  @IsString()
  @IsOptional()
  doctype: string;

  @ValidateNested()
  @Type(() => CourseTopicsDto)
  topics: CourseTopicsDto[];
}
export class CourseTopicsDto {
  @IsString()
  @IsOptional()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsString()
  @IsOptional()
  topic: string;

  @IsString()
  @IsOptional()
  topic_name: string;

  @IsString()
  @IsOptional()
  doctype: string;
}
