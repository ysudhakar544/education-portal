import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Course extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  course_name: string;

  @Column()
  department: string;

  @Column()
  doctype: string;

  @Column()
  topics: Topics[];

  @Column()
  isSynced: boolean;
}
export class Topics {
  name: string;
  docstatus: number;
  topic: string;
  topic_name: string;
  doctype: string;
}
