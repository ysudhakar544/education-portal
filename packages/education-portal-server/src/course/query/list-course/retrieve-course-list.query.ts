import { IQuery } from '@nestjs/cqrs';

export class RetrieveCourseListQuery implements IQuery {
  constructor(
    public offset: number,
    public limit: number,
    public search: string,
    public sort: string,
    public name: string,
    public clientHttpRequest: any,
  ) {}
}
