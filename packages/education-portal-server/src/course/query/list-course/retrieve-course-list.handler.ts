import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveCourseListQuery } from './retrieve-course-list.query';
import { CourseAggregateService } from '../../aggregates/course-aggregate/course-aggregate.service';

@QueryHandler(RetrieveCourseListQuery)
export class RetrieveCourseListQueryHandler
  implements IQueryHandler<RetrieveCourseListQuery> {
  constructor(private readonly manager: CourseAggregateService) {}
  async execute(query: RetrieveCourseListQuery) {
    const { offset, limit, search, sort, name, clientHttpRequest } = query;
    return await this.manager.getCourseList(
      Number(offset),
      Number(limit),
      search,
      sort,
      name,
      clientHttpRequest,
    );
  }
}
