import { IQuery } from '@nestjs/cqrs';

export class RetrieveCourseQuery implements IQuery {
  constructor(public readonly uuid: string, public readonly req: any) {}
}
