import { IEvent } from '@nestjs/cqrs';
import { Attendance } from '../../entity/attendance/attendance.entity';

export class AttendanceUpdatedEvent implements IEvent {
  constructor(public updatePayload: Attendance) {}
}
