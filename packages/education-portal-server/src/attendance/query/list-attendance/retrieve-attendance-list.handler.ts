import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveAttendanceListQuery } from './retrieve-attendance-list.query';
import { AttendanceAggregateService } from '../../aggregates/attendance-aggregate/attendance-aggregate.service';

@QueryHandler(RetrieveAttendanceListQuery)
export class RetrieveAttendanceListQueryHandler
  implements IQueryHandler<RetrieveAttendanceListQuery> {
  constructor(private readonly manager: AttendanceAggregateService) {}
  async execute(query: RetrieveAttendanceListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getAttendanceList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
