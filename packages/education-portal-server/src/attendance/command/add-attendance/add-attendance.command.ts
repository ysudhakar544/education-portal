import { ICommand } from '@nestjs/cqrs';
import { AttendanceDto } from '../../entity/attendance/attendance-dto';

export class AddAttendanceCommand implements ICommand {
  constructor(
    public attendancePayload: AttendanceDto,
    public readonly clientHttpRequest: any,
  ) {}
}
