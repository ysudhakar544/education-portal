import { ICommand } from '@nestjs/cqrs';

export class RemoveAttendanceCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}
