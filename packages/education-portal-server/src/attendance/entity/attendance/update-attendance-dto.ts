import { IsNotEmpty } from 'class-validator';
export class UpdateAttendanceDto {
  @IsNotEmpty()
  uuid: string;
}
