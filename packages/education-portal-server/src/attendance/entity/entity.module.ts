import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Attendance } from './attendance/attendance.entity';
import { AttendanceService } from './attendance/attendance.service';
import { CqrsModule } from '@nestjs/cqrs';

@Module({
  imports: [TypeOrmModule.forFeature([Attendance]), CqrsModule],
  providers: [AttendanceService],
  exports: [AttendanceService],
})
export class AttendanceEntitiesModule {}
