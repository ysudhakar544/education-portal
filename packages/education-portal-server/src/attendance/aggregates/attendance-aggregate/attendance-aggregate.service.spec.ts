import { Test, TestingModule } from '@nestjs/testing';
import { AttendanceAggregateService } from './attendance-aggregate.service';
import { AttendanceService } from '../../entity/attendance/attendance.service';

describe('AttendanceAggregateService', () => {
  let service: AttendanceAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AttendanceAggregateService,
        {
          provide: AttendanceService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AttendanceAggregateService>(
      AttendanceAggregateService,
    );
  });
  AttendanceAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
