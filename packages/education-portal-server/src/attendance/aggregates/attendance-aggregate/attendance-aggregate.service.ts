import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { AttendanceDto } from '../../entity/attendance/attendance-dto';
import { Attendance } from '../../entity/attendance/attendance.entity';
import { AttendanceAddedEvent } from '../../event/attendance-added/attendance-added.event';
import { AttendanceService } from '../../entity/attendance/attendance.service';
import { AttendanceRemovedEvent } from '../../event/attendance-removed/attendance-removed.event';
import { AttendanceUpdatedEvent } from '../../event/attendance-updated/attendance-updated.event';
import { UpdateAttendanceDto } from '../../entity/attendance/update-attendance-dto';

@Injectable()
export class AttendanceAggregateService extends AggregateRoot {
  constructor(private readonly attendanceService: AttendanceService) {
    super();
  }

  addAttendance(attendancePayload: AttendanceDto, clientHttpRequest) {
    const attendance = new Attendance();
    Object.assign(attendance, attendancePayload);
    attendance.uuid = uuidv4();
    this.apply(new AttendanceAddedEvent(attendance, clientHttpRequest));
  }

  async retrieveAttendance(uuid: string, req) {
    const provider = await this.attendanceService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getAttendanceList(offset, limit, sort, search, clientHttpRequest) {
    return await this.attendanceService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.attendanceService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new AttendanceRemovedEvent(found));
  }

  async update(updatePayload: UpdateAttendanceDto) {
    const provider = await this.attendanceService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new AttendanceUpdatedEvent(update));
  }
}
