import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { AttendanceDto } from '../../entity/attendance/attendance-dto';
import { AddAttendanceCommand } from '../../command/add-attendance/add-attendance.command';
import { RemoveAttendanceCommand } from '../../command/remove-attendance/remove-attendance.command';
import { UpdateAttendanceCommand } from '../../command/update-attendance/update-attendance.command';
import { RetrieveAttendanceQuery } from '../../query/get-attendance/retrieve-attendance.query';
import { RetrieveAttendanceListQuery } from '../../query/list-attendance/retrieve-attendance-list.query';
import { UpdateAttendanceDto } from '../../entity/attendance/update-attendance-dto';

@Controller('attendance')
export class AttendanceController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() attendancePayload: AttendanceDto, @Req() req) {
    return this.commandBus.execute(
      new AddAttendanceCommand(attendancePayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveAttendanceCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveAttendanceQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveAttendanceListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateAttendanceDto) {
    return this.commandBus.execute(new UpdateAttendanceCommand(updatePayload));
  }
}
