import { TeacherAddedEventHandler } from './teacher-added/teacher-added.handler';
import { TeacherRemovedEventHandler } from './teacher-removed/teacher.removed.handler';
import { TeacherUpdatedEventHandler } from './teacher-updated/teacher-updated.handler';

export const TeacherEventManager = [
  TeacherAddedEventHandler,
  TeacherRemovedEventHandler,
  TeacherUpdatedEventHandler,
];
