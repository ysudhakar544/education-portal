import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TeacherUpdatedEvent } from './teacher-updated.event';
import { TeacherService } from '../../entity/teacher/teacher.service';

@EventsHandler(TeacherUpdatedEvent)
export class TeacherUpdatedEventHandler
  implements IEventHandler<TeacherUpdatedEvent> {
  constructor(private readonly object: TeacherService) {}

  async handle(event: TeacherUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
