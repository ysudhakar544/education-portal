import { IEvent } from '@nestjs/cqrs';
import { Teacher } from '../../entity/teacher/teacher.entity';

export class TeacherRemovedEvent implements IEvent {
  constructor(public teacher: Teacher) {}
}
