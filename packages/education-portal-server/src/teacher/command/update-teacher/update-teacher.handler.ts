import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateTeacherCommand } from './update-teacher.command';
import { TeacherAggregateService } from '../../aggregates/teacher-aggregate/teacher-aggregate.service';

@CommandHandler(UpdateTeacherCommand)
export class UpdateTeacherCommandHandler
  implements ICommandHandler<UpdateTeacherCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TeacherAggregateService,
  ) {}

  async execute(command: UpdateTeacherCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
