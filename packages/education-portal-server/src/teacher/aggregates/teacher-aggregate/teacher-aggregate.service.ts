import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { TeacherDto } from '../../entity/teacher/teacher-dto';
import { Teacher } from '../../entity/teacher/teacher.entity';
import { TeacherAddedEvent } from '../../event/teacher-added/teacher-added.event';
import { TeacherService } from '../../entity/teacher/teacher.service';
import { TeacherRemovedEvent } from '../../event/teacher-removed/teacher-removed.event';
import { TeacherUpdatedEvent } from '../../event/teacher-updated/teacher-updated.event';
import { UpdateTeacherDto } from '../../entity/teacher/update-teacher-dto';
import { ClientHttpRequestTokenInterface } from '../../../common/client-request-token.interace';

@Injectable()
export class TeacherAggregateService extends AggregateRoot {
  constructor(private readonly teacherService: TeacherService) {
    super();
  }

  addTeacher(
    studentPayload: TeacherDto,
    clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {
    const teacher = new Teacher();
    Object.assign(teacher, studentPayload);
    teacher.uuid = uuidv4();
    this.apply(new TeacherAddedEvent(teacher, clientHttpRequest));
  }

  async retrieveTeacher(uuid: string, req: ClientHttpRequestTokenInterface) {
    const provider = await this.teacherService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getTeacherList(
    offset,
    limit,
    sort,
    search,
    clientHttpRequest: ClientHttpRequestTokenInterface,
  ) {
    return await this.teacherService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.teacherService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new TeacherRemovedEvent(found));
  }

  async update(updatePayload: UpdateTeacherDto) {
    const provider = await this.teacherService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new TeacherUpdatedEvent(update));
  }
}
