import { Test, TestingModule } from '@nestjs/testing';
import { TeacherWebhookAggregateService } from './teacher-webhook-aggregate.service';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';

describe('TeacherWebhookAggregateService', () => {
  let service: TeacherWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TeacherWebhookAggregateService,
        {
          provide: TeacherService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TeacherWebhookAggregateService>(
      TeacherWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
