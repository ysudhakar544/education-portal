import { Injectable, BadRequestException } from '@nestjs/common';
import { TeacherWebhookDto } from '../../../teacher/entity/teacher/teacher-webhook-dto';
import { from, throwError, of } from 'rxjs';
import { TeacherService } from '../../../teacher/entity/teacher/teacher.service';
import { switchMap } from 'rxjs/operators';
import { Teacher } from '../../../teacher/entity/teacher/teacher.entity';
import * as uuidv4 from 'uuid/v4';
import { TEACHER_ALREADY_EXISTS } from '../../../constants/messages';

@Injectable()
export class TeacherWebhookAggregateService {
  constructor(private readonly teacherService: TeacherService) {}

  createdTeacher(teacherPayload: TeacherWebhookDto) {
    return from(
      this.teacherService.findOne({ name: teacherPayload.name }),
    ).pipe(
      switchMap(teacher => {
        if (teacher) {
          return throwError(new BadRequestException(TEACHER_ALREADY_EXISTS));
        }
        const provider = this.mapTeacher(teacherPayload);
        this.teacherService
          .create(provider)
          .then(success => {})
          .catch(err => {});
        return of({});
      }),
    );
  }
  mapTeacher(teacherPayload) {
    const teacher = new Teacher();
    Object.assign(teacher, teacherPayload);
    teacher.isSynced = true;
    teacher.uuid = uuidv4();
    return teacher;
  }

  deletedTeacher(teacherPayload: TeacherWebhookDto) {
    this.teacherService.deleteOne({ name: teacherPayload.name });
  }
  updatedTeacher(teacherPayload: TeacherWebhookDto) {
    return from(
      this.teacherService.findOne({
        name: teacherPayload.name,
      }),
    ).pipe(
      switchMap(teacher => {
        if (!teacher) {
          return this.createdTeacher(teacherPayload);
        }
        teacher.isSynced = true;
        this.teacherService.updateOne(
          { name: teacher.name },
          { $set: teacherPayload },
        );
        return of({});
      }),
    );
  }
}
