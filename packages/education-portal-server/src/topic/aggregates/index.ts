import { TopicAggregateService } from './topic-aggregate/topic-aggregate.service';
import { TopicWebhookAggregateService } from './topic-webhook-aggregate/topic-webhook-aggregate.service';

export const TopicAggregatesManager = [
  TopicAggregateService,
  TopicWebhookAggregateService,
];
