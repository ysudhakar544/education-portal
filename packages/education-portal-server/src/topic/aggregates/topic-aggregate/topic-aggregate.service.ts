import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { TopicDto } from '../../entity/topic/topic-dto';
import { Topic } from '../../entity/topic/topic.entity';
import { TopicAddedEvent } from '../../event/topic-added/topic-added.event';
import { TopicService } from '../../entity/topic/topic.service';
import { TopicRemovedEvent } from '../../event/topic-removed/topic-removed.event';
import { TopicUpdatedEvent } from '../../event/topic-updated/topic-updated.event';
import { UpdateTopicDto } from '../../entity/topic/update-topic-dto';

@Injectable()
export class TopicAggregateService extends AggregateRoot {
  constructor(private readonly topicService: TopicService) {
    super();
  }

  addTopic(topicPayload: TopicDto, clientHttpRequest) {
    const topic = new Topic();
    Object.assign(topic, topicPayload);
    topic.uuid = uuidv4();
    this.apply(new TopicAddedEvent(topic, clientHttpRequest));
  }

  async retrieveTopic(uuid: string, req) {
    const provider = await this.topicService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getTopicList(offset, limit, sort, search, clientHttpRequest) {
    return await this.topicService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.topicService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new TopicRemovedEvent(found));
  }

  async update(updatePayload: UpdateTopicDto) {
    const provider = await this.topicService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new TopicUpdatedEvent(update));
  }
}
