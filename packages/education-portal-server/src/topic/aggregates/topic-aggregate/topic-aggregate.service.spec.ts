import { Test, TestingModule } from '@nestjs/testing';
import { TopicAggregateService } from './topic-aggregate.service';
import { TopicService } from '../../entity/topic/topic.service';

describe('TopicAggregateService', () => {
  let service: TopicAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TopicAggregateService,
        {
          provide: TopicService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TopicAggregateService>(TopicAggregateService);
  });
  TopicAggregateService;
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
