import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import { TopicWebhookDto } from '../../entity/topic/topic-webhook-dto';
import { TopicService } from '../../entity/topic/topic.service';
import { Topic, TopicContentDto } from '../../entity/topic/topic.entity';
import { from, throwError } from 'rxjs';
import { switchMap, map, retry } from 'rxjs/operators';
import { FRAPPE_API_GET_TOPIC_ENDPOINT } from '../../../constants/routes';
import { TOPIC_ALREADY_EXISTS } from '../../../constants/messages';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class TopicWebhookAggregateService {
  constructor(
    private readonly topicService: TopicService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) {}

  topicCreated(topicPayload: TopicWebhookDto) {
    return from(
      this.topicService.findOne({
        name: topicPayload.name,
      }),
    ).pipe(
      switchMap(topic => {
        if (topic) {
          return throwError(new BadRequestException(TOPIC_ALREADY_EXISTS));
        }
        const provider = this.mapTopic(topicPayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.topicService
          .create(provider)
          .then(success => {})
          .catch(error => {});
        return this.syncTopicContent(provider);
      }),
    );
  }

  mapTopic(topicPayload: TopicWebhookDto) {
    const topic = new Topic();
    Object.assign(topic, topicPayload);
    return topic;
  }

  syncTopicContent(topicPayload: Topic) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_TOPIC_ENDPOINT +
              topicPayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                map(res => res.data.data),
                switchMap(response => {
                  const topic_content = this.mapContent(response.topic_content);
                  return from(
                    this.topicService.updateOne(
                      { name: topicPayload.name },
                      {
                        $set: {
                          topic_content,
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  mapContent(topicContent: TopicContentDto[]) {
    const sanitizedData = [];
    topicContent.forEach(eachTopicContent => {
      sanitizedData.push({
        name: eachTopicContent.name,
        docstatus: eachTopicContent.docstatus,
        content_type: eachTopicContent.content_type,
        content: eachTopicContent.content,
        doctype: eachTopicContent.doctype,
      });
    });
    return sanitizedData;
  }

  topicDeleted(topicPayload: TopicWebhookDto) {
    this.topicService.deleteOne({ name: topicPayload.name });
  }

  topicUpdated(topicPayload: TopicWebhookDto) {
    return from(this.topicService.findOne({ name: topicPayload.name })).pipe(
      switchMap(topic => {
        if (!topic) {
          return this.topicCreated(topicPayload);
        }
        topic.isSynced = true;
        this.topicService.updateOne(
          { name: topic.name },
          { $set: topicPayload },
        );
        return this.syncTopicContent(topic);
      }),
    );
  }
}
