import { Test, TestingModule } from '@nestjs/testing';
import { TopicWebhookAggregateService } from './topic-webhook-aggregate.service';
import { TopicService } from '../../entity/topic/topic.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('TopicWebhookAggregateService', () => {
  let service: TopicWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TopicWebhookAggregateService,
        {
          provide: TopicService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TopicWebhookAggregateService>(
      TopicWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
