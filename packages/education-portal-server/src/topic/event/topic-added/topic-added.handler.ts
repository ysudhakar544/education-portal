import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TopicAddedEvent } from './topic-added.event';
import { TopicService } from '../../entity/topic/topic.service';

@EventsHandler(TopicAddedEvent)
export class TopicAddedCommandHandler
  implements IEventHandler<TopicAddedEvent> {
  constructor(private readonly topicService: TopicService) {}
  async handle(event: TopicAddedEvent) {
    const { topic } = event;
    await this.topicService.create(topic);
  }
}
