import { InjectRepository } from '@nestjs/typeorm';
import { Topic } from './topic.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class TopicService {
  constructor(
    @InjectRepository(Topic)
    private readonly topicRepository: MongoRepository<Topic>,
  ) {}

  async find(query?) {
    return await this.topicRepository.find(query);
  }

  async create(topic: Topic) {
    const topicObject = new Topic();
    Object.assign(topicObject, topic);
    return await this.topicRepository.insertOne(topicObject);
  }

  async findOne(param, options?) {
    return await this.topicRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.topicRepository.manager.connection
      .getMetadata(Topic)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.topicRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.topicRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.topicRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.topicRepository.updateOne(query, options);
  }
}
