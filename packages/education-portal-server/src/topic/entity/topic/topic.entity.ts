import { Column, ObjectIdColumn, BaseEntity, ObjectID, Entity } from 'typeorm';

@Entity()
export class Topic extends BaseEntity {
  @ObjectIdColumn()
  _id: ObjectID;

  @Column()
  uuid: string;

  @Column()
  name: string;

  @Column()
  docstatus: number;

  @Column()
  topic_name: string;

  @Column()
  doctype: string;

  @Column()
  topic_content: TopicContentDto[];

  @Column()
  isSynced: boolean;
}
export class TopicContentDto {
  name: string;
  docstatus: number;
  content_type: string;
  content: string;
  doctype: string;
}
