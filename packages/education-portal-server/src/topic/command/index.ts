import { AddTopicCommandHandler } from './add-topic/add-topic.handler';
import { RemoveTopicCommandHandler } from './remove-topic/remove-topic.handler';
import { UpdateTopicCommandHandler } from './update-topic/update-topic.handler';

export const TopicCommandManager = [
  AddTopicCommandHandler,
  RemoveTopicCommandHandler,
  UpdateTopicCommandHandler,
];
