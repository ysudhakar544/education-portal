import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateTopicCommand } from './update-topic.command';
import { TopicAggregateService } from '../../aggregates/topic-aggregate/topic-aggregate.service';

@CommandHandler(UpdateTopicCommand)
export class UpdateTopicCommandHandler
  implements ICommandHandler<UpdateTopicCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: TopicAggregateService,
  ) {}

  async execute(command: UpdateTopicCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
