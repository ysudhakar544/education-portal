import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateVideoCommand } from './update-video.command';
import { VideoAggregateService } from '../../aggregates/video-aggregate/video-aggregate.service';

@CommandHandler(UpdateVideoCommand)
export class UpdateVideoCommandHandler
  implements ICommandHandler<UpdateVideoCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: VideoAggregateService,
  ) {}

  async execute(command: UpdateVideoCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
