import { Test, TestingModule } from '@nestjs/testing';
import { VideoWebhookAggregateService } from './video-webhook-aggregate.service';
import { VideoService } from '../../../video/entity/video/video.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('VideoWebhookAggregateService', () => {
  let service: VideoWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        VideoWebhookAggregateService,
        {
          provide: VideoService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<VideoWebhookAggregateService>(
      VideoWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
