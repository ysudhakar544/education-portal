import {
  Injectable,
  BadRequestException,
  HttpService,
  NotImplementedException,
} from '@nestjs/common';
import { VideoWebhookDto } from '../../entity/video/video-webhook-dto';
import { VideoService } from '../../../video/entity/video/video.service';
import { Video } from '../../../video/entity/video/video.entity';
import { from, throwError } from 'rxjs';
import { switchMap, retry } from 'rxjs/operators';
import { VIDEO_ALREADY_EXISTS } from '../../../constants/messages';

import * as uuidv4 from 'uuid/v4';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import { FRAPPE_API_GET_VIDEO_ENDPOINT } from '../../../constants/routes';
@Injectable()
export class VideoWebhookAggregateService {
  constructor(
    private readonly videoService: VideoService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) {}

  videoCreated(videoPayload: VideoWebhookDto) {
    return from(
      this.videoService.findOne({
        name: videoPayload.name,
      }),
    ).pipe(
      switchMap(video => {
        if (video) {
          return throwError(new BadRequestException(VIDEO_ALREADY_EXISTS));
        }
        const provider = this.mapVideo(videoPayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.videoService
          .create(provider)
          .then(success => {})
          .catch(err => {});
        return this.syncVideo(provider);
      }),
    );
  }

  mapVideo(videoPayload: VideoWebhookDto) {
    const video = new Video();
    Object.assign(video, videoPayload);
    return video;
  }

  syncVideo(videoPayload: Video) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_VIDEO_ENDPOINT +
              videoPayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                switchMap(() => {
                  return from(
                    this.videoService.updateOne(
                      { name: videoPayload.name },
                      {
                        $set: {
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  videoDeleted(videoPayload: VideoWebhookDto) {
    this.videoService.deleteOne({ name: videoPayload.name });
  }

  videoUpdated(videoPayload: VideoWebhookDto) {
    return from(this.videoService.findOne({ name: videoPayload.name })).pipe(
      switchMap(video => {
        if (!video) {
          return this.videoCreated(videoPayload);
        }
        video.isSynced = true;
        this.videoService.updateOne(
          { name: video.name },
          { $set: videoPayload },
        );
        return this.syncVideo(video);
      }),
    );
  }
}
