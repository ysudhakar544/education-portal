import { IsNotEmpty } from 'class-validator';
export class UpdateVideoDto {
  @IsNotEmpty()
  uuid: string;
}
