import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { RetrieveQuestionListQuery } from './retrieve-question-list.query';
import { QuestionAggregateService } from '../../aggregates/question-aggregate/question-aggregate.service';

@QueryHandler(RetrieveQuestionListQuery)
export class RetrieveQuestionListQueryHandler
  implements IQueryHandler<RetrieveQuestionListQuery> {
  constructor(private readonly manager: QuestionAggregateService) {}
  async execute(query: RetrieveQuestionListQuery) {
    const { offset, limit, search, sort, clientHttpRequest } = query;
    return await this.manager.getQuestionList(
      Number(offset),
      Number(limit),
      search,
      sort,
      clientHttpRequest,
    );
  }
}
