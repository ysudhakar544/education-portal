import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  Body,
  ValidationPipe,
  Req,
  Param,
  Get,
  Query,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { TokenGuard } from '../../../auth/guards/token.guard';
import { QuestionDto } from '../../entity/question/question-dto';
import { AddQuestionCommand } from '../../command/add-question/add-question.command';
import { RemoveQuestionCommand } from '../../command/remove-question/remove-question.command';
import { UpdateQuestionCommand } from '../../command/update-question/update-question.command';
import { RetrieveQuestionQuery } from '../../query/get-question/retrieve-question.query';
import { RetrieveQuestionListQuery } from '../../query/list-question/retrieve-question-list.query';
import { UpdateQuestionDto } from '../../entity/question/update-question-dto';

@Controller('question')
export class QuestionController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() questionPayload: QuestionDto, @Req() req) {
    return this.commandBus.execute(
      new AddQuestionCommand(questionPayload, req),
    );
  }

  @Post('v1/remove/:uuid')
  @UseGuards(TokenGuard)
  remove(@Param('uuid') uuid: string) {
    return this.commandBus.execute(new RemoveQuestionCommand(uuid));
  }

  @Get('v1/get/:uuid')
  @UseGuards(TokenGuard)
  async getClient(@Param('uuid') uuid: string, @Req() req) {
    return await this.queryBus.execute(new RetrieveQuestionQuery(uuid, req));
  }

  @Get('v1/list')
  @UseGuards(TokenGuard)
  getClientList(
    @Query('offset') offset = 0,
    @Query('limit') limit = 10,
    @Query('search') search = '',
    @Query('sort') sort,
    @Req() clientHttpRequest,
  ) {
    if (sort !== 'ASC') {
      sort = 'DESC';
    }
    return this.queryBus.execute(
      new RetrieveQuestionListQuery(
        offset,
        limit,
        sort,
        search,
        clientHttpRequest,
      ),
    );
  }

  @Post('v1/update')
  @UseGuards(TokenGuard)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  updateClient(@Body() updatePayload: UpdateQuestionDto) {
    return this.commandBus.execute(new UpdateQuestionCommand(updatePayload));
  }
}
