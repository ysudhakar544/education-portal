import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import {
  QuestionWebhookDto,
  OptionsWebhookDto,
} from '../../entity/question/question-webhook-dto';
import { QuestionService } from '../../entity/question/question.service';
import { Question } from '../../entity/question/question.entity';
import { from, throwError } from 'rxjs';
import { switchMap, map, retry } from 'rxjs/operators';
import { FRAPPE_API_GET_QUESTION_ENDPOINT } from '../../../constants/routes';
import { QUESTION_ALREADY_EXISTS } from '../../../constants/messages';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class QuestionWebhookAggregateService {
  constructor(
    private readonly questionService: QuestionService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) {}

  questionCreated(questionPayload: QuestionWebhookDto) {
    return from(
      this.questionService.findOne({
        name: questionPayload.name,
      }),
    ).pipe(
      switchMap(question => {
        if (question) {
          return throwError(new BadRequestException(QUESTION_ALREADY_EXISTS));
        }
        const provider = this.mapQuestion(questionPayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.questionService
          .create(provider)
          .then(success => {})
          .catch(error => {});
        return this.syncGeneralChild(provider);
      }),
    );
  }

  mapQuestion(questionPayload: QuestionWebhookDto) {
    const question = new Question();
    Object.assign(question, questionPayload);
    return question;
  }

  syncGeneralChild(questionPayload: Question) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_QUESTION_ENDPOINT +
              questionPayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                map(res => res.data.data),
                switchMap(response => {
                  const options = this.mapOptions(response.options);
                  return from(
                    this.questionService.updateOne(
                      { name: questionPayload.name },
                      {
                        $set: {
                          options,
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  mapOptions(options: OptionsWebhookDto[]) {
    const sanitizedData = [];
    options.forEach(eachOption => {
      sanitizedData.push({
        name: eachOption.name,
        docstatus: eachOption.docstatus,
        option: eachOption.option,
        is_correct: eachOption.is_correct,
        doctype: eachOption.doctype,
      });
    });
    return sanitizedData;
  }

  questionDeleted(questionPayload: QuestionWebhookDto) {
    this.questionService.deleteOne({ name: questionPayload.name });
  }

  questionUpdated(questionPayload: QuestionWebhookDto) {
    return from(
      this.questionService.findOne({ name: questionPayload.name }),
    ).pipe(
      switchMap(question => {
        if (!question) {
          return this.questionCreated(questionPayload);
        }
        question.isSynced = true;
        this.questionService.updateOne(
          { name: question.name },
          { $set: questionPayload },
        );
        return this.syncGeneralChild(question);
      }),
    );
  }
}
