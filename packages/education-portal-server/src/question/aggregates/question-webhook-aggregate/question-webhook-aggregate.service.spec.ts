import { Test, TestingModule } from '@nestjs/testing';
import { QuestionWebhookAggregateService } from './question-webhook-aggregate.service';
import { QuestionService } from '../../entity/question/question.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('QuestionWebhookAggregateService', () => {
  let service: QuestionWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        QuestionWebhookAggregateService,
        {
          provide: QuestionService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<QuestionWebhookAggregateService>(
      QuestionWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
