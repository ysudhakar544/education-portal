import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import * as uuidv4 from 'uuid/v4';
import { QuestionDto } from '../../entity/question/question-dto';
import { Question } from '../../entity/question/question.entity';
import { QuestionAddedEvent } from '../../event/question-added/question-added.event';
import { QuestionService } from '../../entity/question/question.service';
import { QuestionRemovedEvent } from '../../event/question-removed/question-removed.event';
import { QuestionUpdatedEvent } from '../../event/question-updated/question-updated.event';
import { UpdateQuestionDto } from '../../entity/question/update-question-dto';

@Injectable()
export class QuestionAggregateService extends AggregateRoot {
  constructor(private readonly questionService: QuestionService) {
    super();
  }

  addQuestion(questionPayload: QuestionDto, clientHttpRequest) {
    const question = new Question();
    Object.assign(question, questionPayload);
    question.uuid = uuidv4();
    this.apply(new QuestionAddedEvent(question, clientHttpRequest));
  }

  async retrieveQuestion(uuid: string, req) {
    const provider = await this.questionService.findOne({ uuid });
    if (!provider) throw new NotFoundException();
    return provider;
  }

  async getQuestionList(offset, limit, sort, search, clientHttpRequest) {
    return await this.questionService.list(offset, limit, search, sort);
  }

  async remove(uuid: string) {
    const found = await this.questionService.findOne({ uuid });
    if (!found) {
      throw new NotFoundException();
    }
    this.apply(new QuestionRemovedEvent(found));
  }

  async update(updatePayload: UpdateQuestionDto) {
    const provider = await this.questionService.findOne({
      uuid: updatePayload.uuid,
    });
    if (!provider) {
      throw new NotFoundException();
    }
    const update = Object.assign(provider, updatePayload);
    this.apply(new QuestionUpdatedEvent(update));
  }
}
