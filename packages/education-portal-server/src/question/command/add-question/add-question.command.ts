import { ICommand } from '@nestjs/cqrs';
import { QuestionDto } from '../../entity/question/question-dto';

export class AddQuestionCommand implements ICommand {
  constructor(
    public questionPayload: QuestionDto,
    public readonly clientHttpRequest: any,
  ) {}
}
