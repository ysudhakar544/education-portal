import { InjectRepository } from '@nestjs/typeorm';
import { Question } from './question.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class QuestionService {
  constructor(
    @InjectRepository(Question)
    private readonly questionRepository: MongoRepository<Question>,
  ) {}

  async find(query?) {
    return await this.questionRepository.find(query);
  }

  async create(question: Question) {
    const questionObject = new Question();
    Object.assign(questionObject, question);
    return await this.questionRepository.insertOne(questionObject);
  }

  async findOne(param, options?) {
    return await this.questionRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.questionRepository.manager.connection
      .getMetadata(Question)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.questionRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.questionRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.questionRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.questionRepository.updateOne(query, options);
  }
}
