import { IsNotEmpty } from 'class-validator';
export class UpdateQuestionDto {
  @IsNotEmpty()
  uuid: string;
}
