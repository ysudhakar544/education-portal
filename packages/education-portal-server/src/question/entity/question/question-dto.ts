import {
  IsOptional,
  IsString,
  IsNumber,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';

export class QuestionDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  question: string;

  @IsOptional()
  @IsString()
  question_type: string;

  @IsOptional()
  @IsString()
  doctype: string;

  @ValidateNested()
  @Type(() => OptionsDto)
  options: OptionsDto[];
}
export class OptionsDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  docstatus: number;

  @IsOptional()
  @IsString()
  option: string;

  @IsOptional()
  @IsNumber()
  is_correct: number;

  @IsOptional()
  @IsString()
  doctype: string;
}
