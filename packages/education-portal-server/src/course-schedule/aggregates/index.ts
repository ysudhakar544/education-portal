import { CourseScheduleAggregateService } from './course-schedule-aggregate/course-schedule-aggregate.service';
import { CourseScheduleWebhookAggregateService } from './course-schedule-webhook-aggregate/course-schedule-webhook-aggregate.service';

export const CourseScheduleAggregatesManager = [
  CourseScheduleAggregateService,
  CourseScheduleWebhookAggregateService,
];
