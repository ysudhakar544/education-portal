import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { UpdateCourseScheduleCommand } from './update-course-schedule.command';
import { CourseScheduleAggregateService } from '../../aggregates/course-schedule-aggregate/course-schedule-aggregate.service';

@CommandHandler(UpdateCourseScheduleCommand)
export class UpdateCourseScheduleCommandHandler
  implements ICommandHandler<UpdateCourseScheduleCommand> {
  constructor(
    private publisher: EventPublisher,
    private manager: CourseScheduleAggregateService,
  ) {}

  async execute(command: UpdateCourseScheduleCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.update(updatePayload);
    aggregate.commit();
  }
}
