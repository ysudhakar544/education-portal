import { IsNotEmpty } from 'class-validator';
export class UpdateCourseScheduleDto {
  @IsNotEmpty()
  uuid: string;
}
