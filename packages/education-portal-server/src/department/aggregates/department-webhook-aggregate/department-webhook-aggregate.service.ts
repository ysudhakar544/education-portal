import {
  Injectable,
  BadRequestException,
  NotImplementedException,
  HttpService,
} from '@nestjs/common';
import {
  DepartmentWebhookDto,
  ApproversWebookDto,
} from '../../entity/department/department-webhook-dto';
import { DepartmentService } from '../../../department/entity/department/department.service';
import { Department } from '../../../department/entity/department/department.entity';
import { from, throwError } from 'rxjs';
import { switchMap, map, retry } from 'rxjs/operators';
import { FRAPPE_API_GET_DEPARTMENT_ENDPOINT } from '../../../constants/routes';
import { DEPARTMENT_ALREADY_EXISTS } from '../../../constants/messages';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';
import * as uuidv4 from 'uuid/v4';
@Injectable()
export class DepartmentWebhookAggregateService {
  constructor(
    private readonly departmentService: DepartmentService,
    private readonly settingsService: SettingsService,
    private readonly http: HttpService,
    private readonly clientTokenManager: ClientTokenManagerService,
  ) {}

  departmentCreated(departmentPayload: DepartmentWebhookDto) {
    return from(
      this.departmentService.findOne({
        name: departmentPayload.name,
      }),
    ).pipe(
      switchMap(department => {
        if (department) {
          return throwError(new BadRequestException(DEPARTMENT_ALREADY_EXISTS));
        }
        const provider = this.mapDepartment(departmentPayload);
        provider.uuid = uuidv4();
        provider.isSynced = false;
        this.departmentService
          .create(provider)
          .then(success => {})
          .catch(error => {});
        return this.syncDepartmentApprovers(provider);
      }),
    );
  }

  mapDepartment(departmentPayload: DepartmentWebhookDto) {
    const department = new Department();
    Object.assign(department, departmentPayload);
    return department;
  }

  syncDepartmentApprovers(departmentPayload: Department) {
    return this.settingsService.find().pipe(
      switchMap(settings => {
        if (!settings.authServerURL) {
          return throwError(new NotImplementedException());
        }
        return this.clientTokenManager.getClientToken().pipe(
          switchMap(token => {
            const url =
              settings.authServerURL +
              FRAPPE_API_GET_DEPARTMENT_ENDPOINT +
              departmentPayload.name;
            return this.http
              .get(url, {
                headers: this.settingsService.getAuthorizationHeaders(token),
              })
              .pipe(
                map(res => res.data.data),
                switchMap(response => {
                  const leaveApprovers = this.mapApprovers(
                    response.leave_approvers,
                  );
                  const expenseApprovers = this.mapApprovers(
                    response.expense_approvers,
                  );
                  return from(
                    this.departmentService.updateOne(
                      { name: departmentPayload.name },
                      {
                        $set: {
                          leave_approvers: leaveApprovers,
                          expense_approvers: expenseApprovers,
                          isSynced: true,
                        },
                      },
                    ),
                  );
                }),
              );
          }),
          retry(3),
        );
      }),
    );
  }

  mapApprovers(Approver: ApproversWebookDto[]) {
    const sanitizedData = [];
    Approver.forEach(eachApprover => {
      sanitizedData.push({
        name: eachApprover.name,
        docstatus: eachApprover.docstatus,
        approver: eachApprover.approver,
        doctype: eachApprover.doctype,
      });
    });
    return sanitizedData;
  }

  departmentDeleted(departmentPayload: DepartmentWebhookDto) {
    this.departmentService.deleteOne({ name: departmentPayload.name });
  }

  departmentUpdated(departmentPayload: DepartmentWebhookDto) {
    return from(
      this.departmentService.findOne({ name: departmentPayload.name }),
    ).pipe(
      switchMap(department => {
        if (!department) {
          return this.departmentCreated(departmentPayload);
        }
        department.isSynced = true;
        this.departmentService.updateOne(
          { name: department.name },
          { $set: departmentPayload },
        );
        return this.syncDepartmentApprovers(department);
      }),
    );
  }
}
