import { Test, TestingModule } from '@nestjs/testing';
import { DepartmentWebhookAggregateService } from './department-webhook-aggregate.service';
import { DepartmentService } from '../../../department/entity/department/department.service';
import { HttpService } from '@nestjs/common';
import { SettingsService } from '../../../system-settings/aggregates/settings/settings.service';
import { ClientTokenManagerService } from '../../../auth/aggregates/client-token-manager/client-token-manager.service';

describe('DepartmentWebhookAggregateService', () => {
  let service: DepartmentWebhookAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        DepartmentWebhookAggregateService,
        {
          provide: DepartmentService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: SettingsService,
          useValue: {},
        },
        {
          provide: ClientTokenManagerService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<DepartmentWebhookAggregateService>(
      DepartmentWebhookAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
