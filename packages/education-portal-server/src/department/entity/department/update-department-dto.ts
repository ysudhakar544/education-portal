import { IsNotEmpty } from 'class-validator';
export class UpdateDepartmentDto {
  @IsNotEmpty()
  uuid: string;
}
