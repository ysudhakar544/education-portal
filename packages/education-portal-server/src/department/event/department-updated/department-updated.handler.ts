import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { DepartmentUpdatedEvent } from './department-updated.event';
import { DepartmentService } from '../../entity/department/department.service';

@EventsHandler(DepartmentUpdatedEvent)
export class DepartmentUpdatedCommandHandler
  implements IEventHandler<DepartmentUpdatedEvent> {
  constructor(private readonly object: DepartmentService) {}

  async handle(event: DepartmentUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}
