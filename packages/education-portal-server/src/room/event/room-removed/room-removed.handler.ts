import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { RoomService } from '../../entity/room/room.service';
import { RoomRemovedEvent } from './room-removed.event';

@EventsHandler(RoomRemovedEvent)
export class RoomRemovedCommandHandler
  implements IEventHandler<RoomRemovedEvent> {
  constructor(private readonly roomService: RoomService) {}
  async handle(event: RoomRemovedEvent) {
    const { room } = event;
    await this.roomService.deleteOne({ uuid: room.uuid });
  }
}
