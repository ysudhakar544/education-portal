import { InjectRepository } from '@nestjs/typeorm';
import { Room } from './room.entity';
import { Injectable } from '@nestjs/common';
import { MongoRepository } from 'typeorm';

@Injectable()
export class RoomService {
  constructor(
    @InjectRepository(Room)
    private readonly roomRepository: MongoRepository<Room>,
  ) {}

  async find(query?) {
    return await this.roomRepository.find(query);
  }

  async create(room: Room) {
    const roomObject = new Room();
    Object.assign(roomObject, room);
    return await this.roomRepository.insertOne(roomObject);
  }

  async findOne(param, options?) {
    return await this.roomRepository.findOne(param, options);
  }

  async list(skip, take, search, sort) {
    const nameExp = new RegExp(search, 'i');
    const columns = this.roomRepository.manager.connection
      .getMetadata(Room)
      .ownColumns.map(column => column.propertyName);

    const $or = columns.map(field => {
      const filter = {};
      filter[field] = nameExp;
      return filter;
    });
    const $and: any[] = [{ $or }];

    const where: { $and: any } = { $and };

    const results = await this.roomRepository.find({
      skip,
      take,
      where,
    });

    return {
      docs: results || [],
      length: await this.roomRepository.count(where),
      offset: skip,
    };
  }

  async deleteOne(query, options?) {
    return await this.roomRepository.deleteOne(query, options);
  }

  async updateOne(query, options?) {
    return await this.roomRepository.updateOne(query, options);
  }
}
