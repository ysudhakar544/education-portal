# Setup backend repository
- ```git clone https://gitlab.com/aartzy/education-portal.git```
- ```cd education-portal/packages/education-portal-server```
- ```npm i```
- Create a ```.env``` file
- Paste this in ```.env``` file

```
DB_USER=education-portal
DB_PASSWORD=admin
DB_NAME=education-portal
DB_HOST=localhost
CACHE_DB_NAME=user-cache
CACHE_DB_USER=user-cache
CACHE_DB_PASSWORD=admin
```
# Setup MongoDB

## If MongoDb container exist
- ```docker ps```
- ```docker start {{MongoContainerName}}```
- Create 2 users in mongodb
```
mongo education-portal --host localhost --port 27017 -u root -p admin --authenticationDatabase admin --eval "db.createUser({user: 'education-portal', pwd: 'admin', roles:[{role:'dbOwner', db: 'education-portal'}], mechanisms:['SCRAM-SHA-1']});" 
```
```
mongo user-cache --host localhost --port 27017 -u root -p admin --authenticationDatabase admin --eval "db.createUser({user: 'user-cache', pwd: 'admin', roles:[{role:'dbOwner', db: 'user-cache'}], mechanisms:['SCRAM-SHA-1']});"

```

# Start Backend

- ``` npm run start:debug ```

# Setup backend with Auth server

![Screenshot_from_2020-02-05_12-28-18](https://gitlab.com/aartzy/education-portal/-/wikis/uploads/eec96b7668947664f4e58282bb8bfdac/Screenshot_from_2020-02-05_12-28-18.png)